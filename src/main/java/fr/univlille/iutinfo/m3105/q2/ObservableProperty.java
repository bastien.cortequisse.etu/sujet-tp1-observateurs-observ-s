package fr.univlille.iutinfo.m3105.q2;

import fr.univlille.iutinfo.m3105.q1.Subject;

public class ObservableProperty extends Subject{
	private Object o;
	public Object getValue() {
		
		return this.o;
	}
	
	public void setValue(Object val) {
		this.o=val;
		this.notifyObservers(val);
	}

}

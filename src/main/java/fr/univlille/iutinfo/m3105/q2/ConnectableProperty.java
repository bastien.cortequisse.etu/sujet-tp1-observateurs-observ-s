package fr.univlille.iutinfo.m3105.q2;

import fr.univlille.iutinfo.m3105.q1.Observer;
import fr.univlille.iutinfo.m3105.q1.Subject;

public class ConnectableProperty extends ObservableProperty implements Observer{

	public void connectTo(ConnectableProperty p2) {
		p2.attach(this);
		this.setValue(p2.getValue());
	}

	public void biconnectTo(ConnectableProperty p2) {
		this.attach(p2);
		connectTo(p2);
	}

	public void unconnectFrom(ConnectableProperty p2) {
		p2.detach(this);
	}

	@Override
	public void update(Subject subj){
		// TODO Auto-generated method stub
	}

	@Override
	public void update(Subject subj, Object data) {
		if(this.getValue()!=data) {
			this.setValue(data);
		}
		}

}

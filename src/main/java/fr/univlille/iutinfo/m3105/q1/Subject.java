package fr.univlille.iutinfo.m3105.q1;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {
	public List<Observer> observers = new ArrayList<>();

	public void attach(Observer obs) {
		this.observers.add(obs);
	}

	public void detach(Observer obs) {
		this.observers.remove(obs);
	}

	public void notifyObservers() {
		if (this.observers != null) {
			for (int i = 0; i < this.observers.size(); i++) {
				this.observers.get(i).update(this);
			}
		}
	}

	public void notifyObservers(Object data) {
		if (this.observers != null) {
			for (int i = 0; i < this.observers.size(); i++) {
				this.observers.get(i).update(this, data);
			}
		}
	}

}

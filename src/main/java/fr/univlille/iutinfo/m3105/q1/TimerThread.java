package fr.univlille.iutinfo.m3105.q1;

public class TimerThread extends Thread {
	private Timer timer;
	
	
	public TimerThread(Timer timer) {
		this.timer = timer;
	}


	public void run() {
		while (true) {
			try {
				sleep(1000);
				this.timer.notifyObservers();
				// annoncer le « tick-horloge »
			} catch (InterruptedException e) {
				// on ignore et on espère que ce n’est pas grave
			}
		}
	}
}
